package counter

import (
	"time"

	"bitbucket.org/benoit-leffray/httpcounter/persister"
	"bitbucket.org/benoit-leffray/httpcounter/windower"
	"github.com/pkg/errors"
)

// New returns an initialized counter
func New(persister persister.Persister, windower windower.Windower) *SlidingCounter {
	return &SlidingCounter{
		persister: persister,
		windower:  windower,
	}
}

// SlidingCounter is the main struct for the counter
type SlidingCounter struct {
	persister persister.Persister
	windower  windower.Windower
}

// Open starts the counter
func (ch *SlidingCounter) Open() error {
	events := new([]time.Time)

	// reload persisted events
	if err := ch.persister.Read(&events); err != nil {
		return errors.Wrap(err, "can't read events")
	}

	// open window and reload events
	ch.windower.Open(events)

	return nil
}

// Close closes the counter
func (ch *SlidingCounter) Close() error {
	// close window and retrieve the currently held events
	events := ch.windower.Close()

	// persist the events now
	if err := ch.persister.Write(events); err != nil {
		return errors.Wrap(err, "can't write events")
	}

	if err := ch.persister.Close(); err != nil {
		return errors.Wrap(err, "can't close persister")
	}

	return nil
}

// Increment increments the counter and returns the updated value
func (ch *SlidingCounter) Increment() int {
	return ch.windower.Add()
}
