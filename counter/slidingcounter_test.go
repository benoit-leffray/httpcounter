package counter

import (
	"bytes"
	"testing"
	"time"

	"bitbucket.org/benoit-leffray/httpcounter/persister"
	"bitbucket.org/benoit-leffray/httpcounter/windower"
)

func TestCounter(t *testing.T) {
	for _, test := range []struct {
		nbEvents int
		expected int
	}{
		{
			nbEvents: 10,
			expected: 11,
		},
		{
			nbEvents: 1,
			expected: 2,
		},
	} {
		var buf bytes.Buffer

		store := &persister.MockStore{
			Buffer: buf,
		}

		p := persister.New(store)

		var events []time.Time

		for i := 0; i < test.nbEvents; i++ {
			events = append(events, time.Now())
		}

		p.Write(events)

		w := windower.New(3*time.Second, time.Second)
		c := New(p, w)

		if err := c.Open(); err != nil {
			t.Errorf("open counter, got: %v, want: %v.", err, nil)
		}

		count := c.Increment()

		if count != test.expected {
			t.Errorf("increment counter, got: %d, want: %d.", count, test.expected)
		}

		if err := c.Close(); err != nil {
			t.Errorf("close counter, got: %v, want: %v.", err, nil)
		}
	}
}
