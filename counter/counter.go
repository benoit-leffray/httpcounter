package counter

import "bitbucket.org/benoit-leffray/httpcounter/opencloser"

// Counter encapsulates a counter
type Counter interface {
	opencloser.Opener
	opencloser.Closer
	Increment() int
}
