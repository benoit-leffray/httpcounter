# httpcounter

## Structure

The main components are the following:
- The handler package contains the HTTP handler implementation, that shows the value of the counter.  

- The counter package contains all the counting logic. 

- The persister package contains the logic that ensures the counting continuity across restarts.  

- The windower package handles the windowing of the count.  


The handler package serves the HTTP requests and delegates the counting to the counter package.  

In turn, the counter package delegates :
- the counting persistence to the persister package,
- the windowing to the widower package.

## Testing

The unit tests can be run with:
```
go test -race ./...
```

## Running the program

The program can be run with:
```
go run main.go -port 8080 -span 60s -refresh 1s -filename data.dat 
```
The HTTP server then listens to the port 8080,
the sliding window spans 60s and the window is refreshed every second.
The data are persisted in the file 'data.dat'.  

The program can be stopped cleanly by typing any key on the command line.  

The data is then properly persisted. When the program is restarted with the same filename, the persisted data will then be reloaded and the counting continuity is ensured across the restart.


