package windower

import (
	"time"
)

// New return a sliding window
func New(span time.Duration, refreshRate time.Duration) *SlidingWindow {
	return &SlidingWindow{
		span:         span,
		refreshRate:  refreshRate,
		eventCh:      make(chan *eventBlk),
		initialCount: 0,
		stopCh:       make(chan *closeBlk),
	}
}

// SlidingWindow is the main data struct
type SlidingWindow struct {
	span         time.Duration
	refreshRate  time.Duration
	eventCh      chan *eventBlk
	events       []time.Time
	initialCount int
	stopCh       chan *closeBlk
}

// eventBlk is the request block struct used
// to add an event to the window and retrieve
// the current number of events in the window
type eventBlk struct {
	time time.Time
	resp chan int
}

// closeBlk is the request block struct used
// to request the window to stop
type closeBlk struct {
	resp chan struct{}
}

// Add adds a notification to the window
func (sw *SlidingWindow) Add() int {
	event := &eventBlk{
		time: time.Now(),
		resp: make(chan int),
	}

	sw.eventCh <- event
	return <-event.resp
}

func (sw *SlidingWindow) run() {
	go func() {
		ticker := time.NewTicker(sw.refreshRate)
		defer ticker.Stop()

		for {
			select {
			case eventReq := <-sw.eventCh:
				sw.events = append(sw.events, eventReq.time)
				eventReq.resp <- len(sw.events)

			case <-ticker.C:
				sw.removeOldEvents()

			case stopReq := <-sw.stopCh:
				stopReq.resp <- struct{}{}
				return
			}
		}
	}()
}

func (sw *SlidingWindow) removeOldEvents() {
	// determine the timestamp of the oldest event of the
	// current window
	windowStart := time.Now().Add(-sw.span)

	var event time.Time

	// iterate through the older events until the 1st
	// event that belong to the current window
	for _, event = range sw.events {
		if event.Before(windowStart) {
			sw.events = sw.events[1:]
		} else {
			break
		}
	}
}

// Open reload events & starts the window
func (sw *SlidingWindow) Open(events *[]time.Time) {
	sw.events = *events
	sw.run()
}

// Close stops the window and relinquish resources
func (sw *SlidingWindow) Close() []time.Time {
	stop := &closeBlk{
		resp: make(chan struct{}),
	}

	sw.stopCh <- stop
	<-stop.resp

	return sw.events
}
