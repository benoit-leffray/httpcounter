package windower

import (
	"testing"
	"time"
)

func TestAddOne(t *testing.T) {
	windowSpan := 5 * time.Millisecond
	refreshRate := time.Millisecond

	w := New(windowSpan, refreshRate)

	w.Open(new([]time.Time))
	defer w.Close()

	n := w.Add() // add first event in the window
	if n != 1 {
		t.Errorf("added events, got: %d, want: %d.", n, 1)
	}
}

func TestAddMany(t *testing.T) {
	windowSpan := 5 * time.Millisecond
	refreshRate := time.Millisecond

	nbEvent := 10

	w := New(windowSpan, refreshRate)

	w.Open(new([]time.Time))
	defer w.Close()

	var n int

	for i := 0; i < nbEvent; i++ {
		n = w.Add()
	}

	if n != nbEvent {
		t.Errorf("added events, got: %d, want: %d.", n, nbEvent)
	}
}

func TestAddEventRetrieve(t *testing.T) {
	windowSpan := 5 * time.Millisecond
	refreshRate := time.Millisecond

	nbEvent := 10

	w := New(windowSpan, refreshRate)

	w.Open(new([]time.Time))

	for i := 0; i < nbEvent; i++ {
		w.Add()
	}

	events := w.Close()

	if len(events) != nbEvent {
		t.Errorf("waited longer than the window span, got: %d, want: %d.", len(events), nbEvent)
	}
}

func TestAddWaitWindowOver(t *testing.T) {
	windowSpan := 5 * time.Millisecond
	refreshRate := time.Millisecond

	nbEvent := 10

	w := New(windowSpan, refreshRate)

	w.Open(new([]time.Time))

	for i := 0; i < nbEvent; i++ {
		w.Add()
	}

	events := w.Close()

	// wait for the window to slide completely
	time.Sleep(windowSpan + refreshRate)

	w.Open(&events) // reload events from previous run

	// wait for the window to refresh -> this should purge all the events from
	// the previous run
	time.Sleep(2 * refreshRate)

	remainingEvents := w.Close() // by now, all the events should have been purged

	if len(remainingEvents) != 0 {
		t.Errorf("waited longer than the window span, got: %d, want: %d.", len(remainingEvents), 0)
	}
}
