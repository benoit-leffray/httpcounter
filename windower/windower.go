package windower

import (
	"time"
)

// Windower is the generic interface to a time window
type Windower interface {
	Open(events *[]time.Time)
	Close() []time.Time

	// Add adds a notification to the window and retrieve the
	// current number of events in the window
	// The window needs to be opened when adding events to it
	Add() int
}
