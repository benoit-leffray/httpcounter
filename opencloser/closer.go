package opencloser

// Closer is a generic closer
type Closer interface {
	Close() error
}
