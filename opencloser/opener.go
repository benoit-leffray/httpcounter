package opencloser

// Opener is a generic opener
type Opener interface {
	Open() error
}
