package handler

import (
	"net/http"

	"bitbucket.org/benoit-leffray/httpcounter/opencloser"
)

// Handler is an HTTP handler that needs to be opened & closed
type Handler interface {
	opencloser.Opener
	opencloser.Closer
	http.Handler
}
