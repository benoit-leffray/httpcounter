package handler

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/benoit-leffray/httpcounter/counter"
	"github.com/pkg/errors"
)

const msg = "The counter is: "

// New return an initialized counter handler
func New(counter counter.Counter) *CounterHandler {
	return &CounterHandler{
		counter: counter,
	}
}

// CounterHandler handles the counter
type CounterHandler struct {
	counter counter.Counter
}

// Open starts the counter handler
func (ch *CounterHandler) Open() error {
	if err := ch.counter.Open(); err != nil {
		return errors.Wrap(err, "can't open counter handler")
	}

	return nil
}

// Close closes the counter handler
func (ch *CounterHandler) Close() error {
	if err := ch.counter.Close(); err != nil {
		return errors.Wrap(err, "can't close counter handler")
	}

	return nil
}

// ServeHTTP serves the incoming HTTP requests
func (ch *CounterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	count := ch.counter.Increment()

	if _, err := w.Write([]byte(msg + strconv.Itoa(count))); err != nil {
		log.Printf("HTTP response writer: %s\n", err)
	}
}
