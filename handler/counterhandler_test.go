package handler

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"bitbucket.org/benoit-leffray/httpcounter/counter"
	"bitbucket.org/benoit-leffray/httpcounter/persister"
	"bitbucket.org/benoit-leffray/httpcounter/windower"
)

func TestCounterHandler(t *testing.T) {
	for _, test := range []struct {
		nbEvents int
		expected string
	}{
		{
			nbEvents: 10,
			expected: msg + "11",
		},
		{
			nbEvents: 1,
			expected: msg + "2",
		},
	} {

		var buf bytes.Buffer

		storage := &persister.MockStore{
			Buffer: buf,
		}

		persister := persister.New(storage)

		var events []time.Time

		for i := 0; i < test.nbEvents; i++ {
			events = append(events, time.Now())
		}

		persister.Write(events)

		windower := windower.New(3*time.Second, time.Second)
		counter := counter.New(persister, windower)

		ch := New(counter)

		if err := ch.Open(); err != nil {
			t.Errorf("open counter, got: %v, want: %v.", err, nil)
		}

		req, _ := http.NewRequest("GET", "", nil)
		w := httptest.NewRecorder()
		ch.ServeHTTP(w, req)

		got := w.Body.String()
		if got != test.expected {
			t.Errorf("Got %s, Want %s", got, test.expected)
		}

		if err := ch.Close(); err != nil {
			t.Errorf("close counter, got: %v, want: %v.", err, nil)
		}
	}
}
