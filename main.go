package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/benoit-leffray/httpcounter/counter"
	"bitbucket.org/benoit-leffray/httpcounter/handler"
	"bitbucket.org/benoit-leffray/httpcounter/persister"
	"bitbucket.org/benoit-leffray/httpcounter/windower"
)

var windowSpan time.Duration
var refreshRate time.Duration
var port int
var fileName string

func main() {
	exitCode := 0
	defer func() { os.Exit(exitCode) }()

	flag.Parse()

	// create the file if it doesn't exit, if it exists, don't clobber its contents
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0600) // RW for user only
	if err != nil {
		log.Println("can't open file: " + err.Error())
		exitCode = 1
		return
	}

	p := persister.New(file)
	w := windower.New(windowSpan, refreshRate)
	c := counter.New(p, w)
	ch := handler.New(c)

	if err := ch.Open(); err != nil {
		log.Println("can't open counter handler: " + err.Error())
		exitCode = 1
		return
	}

	defer func() {
		if err := ch.Close(); err != nil {
			log.Println("can't close counter handler: " + err.Error())
			exitCode = 1
		}
	}()

	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: ch}

	go waitInput(server)

	log.Println("Listening...")
	log.Println("Type Enter to stop the program")

	err = server.ListenAndServe()
	if err != http.ErrServerClosed {
		log.Println("can't close server: " + err.Error())
		exitCode = 1
	}
}

func waitInput(server *http.Server) {
	bufio.NewScanner(os.Stdin).Scan()

	fmt.Println("Exit, Bye")
	server.Close()
}

func init() {
	flag.DurationVar(&windowSpan, "span", time.Second*60, "window span")
	flag.DurationVar(&refreshRate, "refresh", time.Second, "window refresh rate")
	flag.IntVar(&port, "port", 8080, "server port")
	flag.StringVar(&fileName, "filename", "data.dat", "file used for persistance")
}
