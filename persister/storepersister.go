package persister

import (
	"encoding/gob"
	"os"

	"github.com/pkg/errors"
)

// New return a store persister
func New(store Storer) *StorePersister {
	return &StorePersister{
		store: store,
	}
}

// StorePersister persist data to its store
type StorePersister struct {
	store Storer
}

// Close closes the counter handler
func (ch *StorePersister) Close() error {
	if err := ch.store.Close(); err != nil {
		return errors.Wrap(err, "can't close storage")
	}

	return nil
}

// Write serializes the object into the file
func (ch *StorePersister) Write(object interface{}) error {
	// clear any prior contents
	if _, err := ch.store.Seek(0, os.SEEK_SET); err != nil {
		return errors.Wrap(err, "can't seek")
	}

	encoder := gob.NewEncoder(ch.store)

	if err := encoder.Encode(object); err != nil {
		return errors.Wrap(err, "can't encode")
	}

	return nil
}

// Read deserializes the object from the file
func (ch *StorePersister) Read(object interface{}) error {
	// invoke Stat() to get the size of the store
	info, err := ch.store.Stat()
	if err != nil {
		return errors.Wrap(err, "can't stat")
	}

	if info.Size() == 0 {
		return nil // empty store, nothing to reload
	}

	decoder := gob.NewDecoder(ch.store)

	if err := decoder.Decode(object); err != nil {
		return errors.Wrap(err, "can't decode")
	}

	return nil
}
