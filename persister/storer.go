package persister

import (
	"io"
	"os"
)

// Storer reads & writes data to a data store
type Storer interface {
	io.Closer
	io.Reader
	io.Writer

	Seek(offset int64, whence int) (int64, error)
	Stat() (os.FileInfo, error)
}
