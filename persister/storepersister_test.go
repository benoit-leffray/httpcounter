package persister

import (
	"bytes"
	"testing"
)

func TestPersister(t *testing.T) {
	var buf bytes.Buffer

	storage := &MockStore{
		Buffer: buf,
	}

	p := New(storage)
	defer p.Close()

	data := "whatever"

	if err := p.Write(data); err != nil {
		t.Errorf("write data failure, got: %v, want: %v.", err, nil)
	}

	var res string

	if err := p.Read(&res); err != nil {
		t.Errorf("retrieve data failure, got: %v, want: %v.", err, nil)
	}

	if data != res {
		t.Errorf("persist then retrieve data, got: %v, want: %v.", res, data)
	}
}
