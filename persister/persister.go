package persister

import (
	"bitbucket.org/benoit-leffray/httpcounter/opencloser"
)

// Persister abstracts away data persistence
type Persister interface {
	opencloser.Closer

	Read(object interface{}) error
	Write(object interface{}) error
}
