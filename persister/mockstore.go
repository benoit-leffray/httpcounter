package persister

import (
	"bytes"
	"os"
	"time"
)

type mockFileInfo struct {
	size int64
}

func (mockFileInfo) Size() int64 {
	return 1
}

func (mockFileInfo) Name() string {
	return "name"
}

func (mockFileInfo) Mode() os.FileMode {
	return 0600
}

func (mockFileInfo) ModTime() time.Time {
	return time.Now()
}

func (mockFileInfo) IsDir() bool {
	return false
}

func (mockFileInfo) Sys() interface{} {
	return nil
}

////////////////

// MockStore is the main struct for this mock implementation
type MockStore struct {
	Buffer bytes.Buffer
}

// Close closes the mock storage
func (ms *MockStore) Close() error {
	return nil
}

// Read retrieves the contents from the storage
func (ms *MockStore) Read(p []byte) (int, error) {
	return ms.Buffer.Read(p)
}

// Write writes binary contents to the storage
func (ms *MockStore) Write(p []byte) (int, error) {
	return ms.Buffer.Write(p)
}

// Seek seeks in the storage
func (ms *MockStore) Seek(offset int64, whence int) (int64, error) {
	return 0, nil
}

// Stat returns meta info on the storage
func (ms *MockStore) Stat() (os.FileInfo, error) {
	return &mockFileInfo{
		size: 1,
	}, nil
}
